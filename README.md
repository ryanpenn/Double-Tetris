# Double-Tetris (记得Star哦)
Double-Tetris 是一款双人俄罗斯方块游戏，我在传统的俄罗斯方块游戏基础上增加了双人配合玩法，增添了一些游戏技巧和乐趣。技术选型 客户端（requireJS+jQuery+socket.io+PixiJS）+ 服务端（SpringBoot+Netty-SocketIO）。

## 截图
![PC](https://gitee.com/ryanpenn/Double-Tetris/raw/master/screenshot/pc.jpeg)
-
![手机](https://gitee.com/ryanpenn/Double-Tetris/raw/master/screenshot/wx.png)

## 功能说明
- 游戏在经典版俄罗斯方块上进行改变,加入双人游戏的玩法,支持局域网游戏（目前只支持2个客户端连接）
- 先接入的客户端为P1, 使用绿色方块, 出现在屏幕左侧; 后接入的客户端为P2, 使用红色方块,出现在屏幕右侧
- 客户端对屏幕分辨率做了自适应处理,支持不同分辨率的设备.
- 同时支持 键盘(PC端) 和 触屏(移动端) 操作
- 键盘: W/A/S/D（或方向键 ↑ ← ↓ →）分别为 旋转/左移/下移/右移, 空格键 为直接下落
- 触屏: 左/右/下 滑手势 分别对应 左移/右移/下移, 上滑手势 为旋转, 触屏暂未支持直接下落

## 运行说明
- 配置服务端:resources/application.properties, 将server.host改为 本机IP（不要使用localhost或127.0.0.1）
- 运行服务端: cn.ryanpenn.game.tetris.Application.main
- 客户端代码在: webapp/tetris-client 目录中
- 客户端配置: js/main.js 中的连接地址IP、端口保持和服务器一致即可（不要使用localhost或127.0.0.1）
- 客户端需要运行多个,建议采用 browsersync 工具运行,方便在移动端看效果
    - 打开命令行, cd 到 tetris-client 目录中
    - 运行 browser-sync start --server --files "js/*.js"
    - 在浏览器中打开 http://本机IP:3000/tetris-client.html （服务端应该会输出日志: P1已连接）
    - 在 另一台电脑或手机 的浏览器中打开上面的地址,游戏就自动开始了
    - 如果需要查看在微信中的显示效果,可以使用 草料二维码生成器 将地址生成二维码,然后通过微信扫一扫就可以了
- 注意:所有设备必须和服务端在同一网段中
- 游戏难度可以在 js/tetris.js 中调整

## 技术说明
- 基于WebSocket实现通信,使用 netty-socketio(服务端) + socket.io.js(客户端) 实现.
- 服务端是基于 Maven 的Java Application,依赖 spring boot, 可以直接运行,不需要部署到Web容器.
- 客户端使用了 require.js 实现模块化,使用 jquery 来简化编码(其实很少用到...)
- 客户端使用了 pixi.js 游戏引擎. 由于本人对这个游戏引擎还不熟悉,只用很少一部分功能,界面是通过该引擎绘制的.

## 版本
- 后端
    - [spring boot 1.5.10.RELEASE](http://mvnrepository.com/artifact/org.springframework.boot/spring-boot-dependencies/1.5.10.RELEASE)
    - [netty-socketio-1.7.13](https://github.com/mrniko/netty-socketio/releases/tag/netty-socketio-1.7.13)
    - [netty-all 4.1.22.Final](http://mvnrepository.com/artifact/io.netty/netty-all/4.1.22.Final)

- 前端
    - [socket.io.min.js 1.7.4](https://cdn.bootcss.com/socket.io/1.7.4/socket.io.min.js)
    - [require.min.js 2.3.5](https://cdn.bootcss.com/require.js/2.3.5/require.min.js)
    - [jquery.min.js 3.3.1](https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js)
    - [pixi.min.js 4.7.0](https://cdn.bootcss.com/pixi.js/4.7.0/pixi.min.js)

## 参考资料
- [socket.io 中文文档](http://www.cnblogs.com/xiezhengcai/p/3956401.html)
- [socket.io cdn](https://cdnjs.com/libraries/socket.io/1.7.4)
- [PIXI 中文文档](https://github.com/Zainking/learningPixi)
- [PIXI 官方示例](http://pixijs.io/examples/#/basics/)
- [Spring Boot 非web应用程序实例](http://blog.csdn.net/lxh18682851338/article/details/78559595)
- [Netty-SocketIO 简单聊天室](http://blog.csdn.net/sun_t89/article/details/52060946)

## 实用工具
- [IntelliJ IDEA](https://www.jetbrains.com/idea/)
- [browsersync 前端调试神器](https://browsersync.io/)
- [草料二维码生成器](https://cli.im/text)

## 备注
- 本项目是在 [对战版俄罗斯方块](https://gitee.com/ryanpenn/Tetris) 项目基础上修改的,基本游戏实现原理可参考该项目.
- 项目还不完善,Bug肯定也不少,但希望能给大家一些参考,欢迎Star和Fork
