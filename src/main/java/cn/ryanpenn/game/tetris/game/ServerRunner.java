package cn.ryanpenn.game.tetris.game;

import com.corundumstudio.socketio.SocketIOServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * ServerRunner
 *
 * @author pennryan
 */
@Component
public class ServerRunner implements CommandLineRunner {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final SocketIOServer server;

    @Autowired
    public ServerRunner(SocketIOServer server) {
        this.server = server;
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("服务端启动...{}:{}", server.getConfiguration().getHostname(), server.getConfiguration().getPort());
        server.start();
    }
}
