package cn.ryanpenn.game.tetris.config;

import cn.ryanpenn.game.tetris.game.LoginAuthorization;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.SpringAnnotationScanner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ServerConfig
 *
 * @author pennryan
 */
@Configuration
public class ServerConfig {

    @Value("${server.host}")
    private String host;

    @Value("${server.port}")
    private Integer port;

    @Bean
    public LoginAuthorization loginAuthorization() {
        return new LoginAuthorization();
    }

    @Bean
    public SocketIOServer socketIOServer() {
        com.corundumstudio.socketio.Configuration config = new com.corundumstudio.socketio.Configuration();
        config.setHostname(host);
        config.setPort(port);

        //该处可以用来进行身份验证
        config.setAuthorizationListener(loginAuthorization());

        final SocketIOServer server = new SocketIOServer(config);
        return server;
    }

    @Bean
    public SpringAnnotationScanner springAnnotationScanner(SocketIOServer socketServer) {
        return new SpringAnnotationScanner(socketServer);
    }
}
